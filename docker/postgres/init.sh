#!/bin/bash

set -e

# Short Form, since it's quite a bit to type
sql_exec="psql --username $POSTGRES_USER --dbname $POSTGRES_DB -c"

# Discord Database Set-Up
$sql_exec "CREATE DATABASE $STORK_DB;"
$sql_exec "GRANT ALL PRIVILEGES ON DATABASE $STORK_DB TO $POSTGRES_USER;"
