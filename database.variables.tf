variable "database_user" {
  type        = string
  description = "Database administrator user"
  default     = "admin"
}

variable "database_password" {
  type        = string
  description = "Database administrator password"
  sensitive   = true
}
