locals {
  ports = var.environment == "develop" ? [5432] : []
}

resource "docker_image" "postgres" {
  name = "${var.project}/postgres"
  //noinspection HCLUnknownBlockType
  build {
    path = "docker/postgres"
    tag  = ["${var.project}/postgres:1.0"]
    label = {
      "author" = "Roman Shchekotov"
    }
  }
}

resource "docker_container" "postgres" {
  image = docker_image.postgres.latest
  name  = "${var.project}-postgres"

  env = [
    # Postgres Admin
    "POSTGRES_USER=${var.database_user}",
    "POSTGRES_PASSWORD=${var.database_password}",

    # Postgres DB
    "STORK_DB=${var.project}",
  ]

  volumes {
    volume_name    = docker_volume.postgres_data.name
    container_path = "/var/lib/postgresql/data"
  }

  # Port-Map to localhost in Dev-Environments
  dynamic "ports" {
    for_each = local.ports
    content {
      internal = ports.value
      external = ports.value
    }
  }

  restart = "always"
}

resource "docker_volume" "postgres_data" {
  name = "${var.project}-postgres-data"
}
