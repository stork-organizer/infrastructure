package com.storkproject.application.rest.event;

import com.storkproject.application.rest.event.dto.EventCreateDto;
import com.storkproject.application.rest.user.UserService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class EventService {
  private final UserService userService;
  private final EventRepository eventRepository;

  public EventService(EventRepository eventRepository, UserService userService) {
    this.eventRepository = eventRepository;
    this.userService = userService;
  }

  public Event createEvent(EventCreateDto data) {
    UUID uuid = UUID.randomUUID();
    Event event = new Event();
    event.setEventId(uuid.toString());
    event.setEventType(data.getEventType());
    event.setTitle(data.getTitle());
    event.setNotes(data.getNotes());
    event.setStartDate(data.getStartDate());
    event.setEndDate(data.getEndDate());
    event.setOrganizer(userService.currentlyRegisteredUser());
    event.setTeam(data.getTeam());
    event.setRecurrence(data.getRecurrence());
    return eventRepository.save(event);
  }

  public Event modifyEvent(String uuid, EventCreateDto data) {
    Event event = eventRepository.findByEventId(uuid);
    event.setEventId(uuid);
    event.setEventType(data.getEventType());
    event.setTitle(data.getTitle());
    event.setNotes(data.getNotes());
    event.setStartDate(data.getStartDate());
    event.setEndDate(data.getEndDate());
    event.setOrganizer(userService.currentlyRegisteredUser());
    event.setTeam(data.getTeam());
    event.setRecurrence(data.getRecurrence());
    return eventRepository.save(event);
  }

  public boolean deleteEvent(String uuid){
    Event event = eventRepository.findByEventId(uuid);
    if(event != null) {
      eventRepository.delete(event);
      return true;
    }
    return false;
  }


  public List<Event> getEventByOrganizer(String userId) {
    return eventRepository.findByOrganizer(userId);
  }
}
