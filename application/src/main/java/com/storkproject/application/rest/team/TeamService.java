package com.storkproject.application.rest.team;

import com.storkproject.application.rest.team.model.TeamData;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeamService {
    List<TeamData> allTeams =
            new ArrayList<>(List.of(new TeamData("6d8a9547-e0c6-41e8-88fe-a581ee03141b", "team1",""),
            new TeamData("50a5a72a-d5ef-4793-bf06-67f92aa8dcf2", "team2","")));

    public List<TeamData> getAllTeams() {
        return allTeams;
    }

    public void editTeam(TeamData edit){
        Optional<TeamData> team = allTeams.stream().filter(item -> item.getId().equals(edit.getId())).findFirst();
        if(team.isPresent()){
            team.get().setDescription(edit.getDescription());
            team.get().setMembers(edit.getMembers());
            team.get().setName(edit.getName());
        }
    }
}
