package com.storkproject.application.rest.user.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@JsonIgnoreProperties({
    "password", "expired",
    "locked", "reset"
})
public class UserData implements UserDetails {
  String uuid;
  String username;
  String mail;
  String password;
  Set<GrantedAuthority> authorities = new HashSet<>();

  private static BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

  boolean expired = false;
  boolean locked = false;
  boolean reset = false;

  public UserData(String username, String mail, String password) {
    this.username = username;
    this.mail = mail;
    this.password = encoder.encode(password);

    this.uuid = UUID.randomUUID().toString();
  }

  public UserData uuid(String uuid) {
    this.uuid = uuid;
    return this;
  }

  public UserData roles(String... roles) {
    this.authorities.clear();
    for(String role : roles) {
      this.authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
    }
    return this;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.authorities;
  }

  @Override
  public String getPassword() {
    return this.password;
  }

  @Override
  public String getUsername() {
    return this.username;
  }

  @Override
  public boolean isAccountNonExpired() {
    return !expired;
  }

  @Override
  public boolean isAccountNonLocked() {
    return !locked;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return !reset;
  }

  @Override
  public boolean isEnabled() {
    return isAccountNonExpired() &&
        isAccountNonLocked() &&
        isCredentialsNonExpired();
  }
}
