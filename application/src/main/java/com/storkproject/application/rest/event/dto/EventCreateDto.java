package com.storkproject.application.rest.event.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.ZonedDateTime;

@Data
@RequiredArgsConstructor
public class EventCreateDto {
  final String eventType;
  final String title;
  final String notes;
  final ZonedDateTime startDate;
  final ZonedDateTime endDate;
  String team;
  String recurrence;
}
