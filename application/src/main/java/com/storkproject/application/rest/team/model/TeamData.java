package com.storkproject.application.rest.team.model;

import com.storkproject.application.rest.user.model.UserData;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class TeamData {
    String id;
    String name;
    String description;
    List<String> members;

    public TeamData(String id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
        members = new ArrayList<>();
    }

    public void addNewMember(String member){
        members.add(member);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }
}
