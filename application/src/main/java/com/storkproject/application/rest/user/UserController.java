package com.storkproject.application.rest.user;

import com.storkproject.application.rest.user.model.UserData;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RestController
public class UserController {
  UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }


  @GetMapping("/users")
  public List<UserData> getUsers() {
    return this.userService.getUsers();
  }

  @CrossOrigin(origins = "*")
  @GetMapping("/userid")
  public String getUser() {
    return this.userService.currentlyRegisteredUser();
  }

  @CrossOrigin(origins = "*")
  @GetMapping("/userdata")
  public String getUserData() {
    return this.userService.currentlyRegisteredUserData();
  }

  @CrossOrigin(origins = "*")
  @PostMapping("/user/{id}/name")
  public void changeUserName(@PathVariable String id, @RequestBody String name) {
    this.userService.renameUser(id,name);
  }

  @CrossOrigin(origins = "*")
  @PostMapping("/user/{id}/mail")
  public void changeUserMail(@PathVariable String id, @RequestBody String mail) {
    this.userService.changeEmail(id,mail);
  }
}
