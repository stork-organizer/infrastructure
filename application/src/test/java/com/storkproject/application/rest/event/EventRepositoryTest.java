package com.storkproject.application.rest.event;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class EventRepositoryTest {
  @Autowired
  private EventRepository repository;

  @Test
  void getUserById() {
    Event event = new Event();
    event.setEventId("9fb42513-e739-4bd8-a279-eb65544df239");
    event.setEventType("Meeting");
    event.setTitle("Sprint Planning");
    event.setNotes("Sprint Planning");
    event.setStartDate(ZonedDateTime.parse("2020-04-01T00:00:00.000Z"));
    event.setEndDate(ZonedDateTime.parse("2020-04-01T00:00:00.000Z"));
    event.setOrganizer("ca54542f-e2f4-4993-bb92-a8105081eb89");

    repository.save(event);
    Optional<Event> found = repository.findById("9fb42513-e739-4bd8-a279-eb65544df239");
    assertTrue(found.isPresent());
    assertEquals(event, found.get());
  }
}